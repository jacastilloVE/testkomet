CREATE TABLE flieload (
	file_load_id int8 NULL,
	filename varchar(100) NULL,
	systemfilename varchar(100) NULL,
	status varchar(100) NULL,
	lastprocessedrow int8 NULL,
	errorqty int8 NULL,
	loadedqty int8 NULL
)ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;


CREATE TABLE truck (
	serial varchar(100) NULL,
	plate varchar(100) NULL,
	maxweight float8 NULL,
	truckid int8 NULL,
	purchasedate timestamp NULL
)ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE t_user (
	name varchar(100) NULL,
	email varchar(100) NULL
)ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;