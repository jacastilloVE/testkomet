package com.test.trucktest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TrucktestbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrucktestbackendApplication.class, args);
	}

}
