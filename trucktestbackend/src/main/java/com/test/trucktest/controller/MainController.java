package com.test.trucktest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.test.trucktest.dto.LoadedFile;
import com.test.trucktest.dto.User;
import com.test.trucktest.service.FileService;
import com.test.trucktest.service.UserService;

@Controller
public class MainController {

	@Autowired
	FileService fileService;
	
	
	@Autowired
	UserService userService;
	
	
	@PutMapping("loadfile")
	@ResponseBody
	public String loadFile(@RequestParam("file") MultipartFile file, ModelMap modelMap) throws IOException {
	    modelMap.addAttribute("file", file);
	    System.out.println("loaded");
	    
	    fileService.storeFile(file.getInputStream(),file.getOriginalFilename());
	    
	    return "";
	}
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE ,value= "files")
	@ResponseBody
	public List<LoadedFile> getFiles(){
		
		List<LoadedFile> u = fileService.getAll();		
		return u;
	}
	
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE ,value= "users")
	@ResponseBody
	public User getUser(){
		User u = userService.get();
		return u;
	}
	
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,value= "users")
	@ResponseBody
	public User update(@RequestBody User user) {
		userService.save(user);
		System.out.println(user.getName());
		return user;
		
	}
	
}
