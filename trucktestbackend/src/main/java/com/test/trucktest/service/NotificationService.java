package com.test.trucktest.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;


/**
 * Notification service, manage email
 * @author jcastillo
 *
 */
@Service
public class NotificationService {

	
	
	@Autowired
    public JavaMailSender emailSender;
 
    public void sendSimpleMessage(String to, String subject, String text) {
        MimeMessage message = emailSender.createMimeMessage();

        try {
			message.setSubject(subject);
			MimeMessageHelper helper;
	        helper = new MimeMessageHelper(message, true);
	        helper.setTo(to);
	        helper.setText(text, true);
	        emailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
        
        
        
    }
}
