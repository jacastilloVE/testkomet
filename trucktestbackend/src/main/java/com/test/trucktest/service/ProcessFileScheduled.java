package com.test.trucktest.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.test.trucktest.dto.LoadedFile;
import com.test.trucktest.dto.LoadedFileStatus;
import com.test.trucktest.dto.Truck;
import com.test.trucktest.dto.User;


/**
 * Manage via batch import process
 * Allows big files storage with low performance
 * @author jcastillo
 *
 */
@Component
public class ProcessFileScheduled {

	private Logger log = LoggerFactory.getLogger(ProcessFileScheduled.class);

	@Autowired
	TruckService truckService;

	@PersistenceContext
	EntityManager em;

	@Autowired
	FileService fileService;
	/**Qty of line for processing*/
	@Value("${row.batch.qty}")
	int processRowBatchQty;

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	public ProcessFileScheduled() {
		// TODO Auto-generated constructor stub
	}

	@Scheduled(fixedDelayString = "5000", initialDelay = 10000)
	public void processFiles() {
		// here process file

		List<LoadedFile> files = fileService.getByStatus(LoadedFileStatus.PROCESSING.getCode());

		if (files.isEmpty()) {// nothing to process
			return;
		}


		log.debug("Init processing");
		LoadedFile ld = files.get(0);
		File targetFile = new File(ld.getSystemFileName());

		ld.getLastProcessedRow();
		try (FileReader fr = new FileReader(targetFile);
				BufferedReader br = new BufferedReader(fr)) {

			String s;
			int rowNo = 0;

			s = br.readLine();
			int qty = 0;

			while (s != null && qty < processRowBatchQty) {
				rowNo++;
				if (rowNo > ld.getLastProcessedRow()) {
					qty++;// for batch qty
					
					Truck t = parseTruck(s);
					
					if (t!=null) {
						truckService.save(t);
						ld.setLoadedQty(ld.getLoadedQty() + 1);
					}else {

						ld.setErrorQty(ld.getErrorQty() + 1);
					}				
					
					ld.setLastProcessedRow(rowNo);
				}
				s = br.readLine();
			}
			if (br.readLine() == null) {
				ld.setStatus(LoadedFileStatus.WAITNOTIFY);
			}
			fileService.save(ld);

			br.close();
		} catch (FileNotFoundException e) {
			log.error("Error al abrir el archivo");
		} catch (IOException e) {
			log.error("Error al leer");
		}

	}

	/**
	 * Parse truck from string
	 * @param raw
	 * @return
	 */
	public Truck parseTruck(String raw) {
		
		String[] truckraw = raw.split(",");
		if (truckraw.length != 4) {
			log.error("Error en linea faltan datos " + raw);
			return null;
		}
		Truck t = new Truck();
		t.setSerial(truckraw[0]);
		t.setPlate(truckraw[1]);
		// verify date;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			t.setPurchaseDate(Timestamp.from(df.parse(truckraw[2]).toInstant()));
			t.setMaxWeight(BigDecimal.valueOf(Double.valueOf(truckraw[3])).setScale(2,
					RoundingMode.HALF_EVEN));
		} catch (ParseException e) {
			log.error("Error en linea" + raw);
			return null;
		}catch (NumberFormatException e) {
			log.error("Error en linea" + raw);
			return null;
		}
		// verify weight	
		
		return t;
	}
	
	
	/**
	 * Process notifications.
	 */
	@Scheduled(fixedDelayString = "5000", initialDelay = 10000)
	public void processNotification() {
		List<LoadedFile> files = fileService.getByStatus(LoadedFileStatus.WAITNOTIFY.getCode());

		if (files.isEmpty()) {// nothing to process
			return;
		}


		log.debug("Init Notification");
		User u = userService.get();

		for (LoadedFile f : files) {
			try {
				
				StringBuilder body = new StringBuilder();
				body.append("<h3><b>Saludos cordiales, ")
				.append(u.getName())
				.append(".</b></h3>\n")
				.append("Le notificamos que su archivo con nombre <b>")
				.append(f.getFileName())
				.append("</b> ya se encuentra procesado. Con los siguientes resultados")
				.append("<br><b>Filas procesadas</b> :")
				.append(f.getLastProcessedRow())
				.append("<br><b>Filas Guardadas</b>  :")
				.append(f.getLoadedQty())
				.append("<br><b>Filas con errores</b>:")
				.append(f.getErrorQty());				
				notificationService.sendSimpleMessage(
						u.getEmail(), 
						"Archivo " + f.getFileName() + " procesado",
						body.toString());
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}

			f.setStatus(LoadedFileStatus.OK);

			fileService.save(f);

		}
	}
}
