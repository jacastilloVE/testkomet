package com.test.trucktest.service;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.test.trucktest.dto.Truck;

@Service
public class TruckService {

	private Logger log = LoggerFactory.getLogger(ProcessFileScheduled.class);
	@PersistenceContext
	EntityManager em;
	
	private static String INSERT = "INSERT INTO truck (truckID, serial, plate, purchasedate, maxweight)" + 
			//" VALUES ((select coalesce(max(truckid),0) + 1 from truck),?,?,?,?)";
			" VALUES (1,?,?,?,?)";
	
	@Transactional
	public Truck save(Truck t) {
				
		
		BigInteger nextID = BigInteger.ONE;
		Query query = em.createNativeQuery("select coalesce(max(truckid),0) + 1 from truck");
		List<BigInteger> res = query.getResultList();
		if (res.size()>0) {
			nextID =  (BigInteger)(res.get(0));
		}
		
		
		
		query = em.createNativeQuery(INSERT);
		int i = 1;
		//query.setParameter(1, 0);
		query.setParameter(i++, t.getSerial());
		query.setParameter(i++, t.getPlate());
		query.setParameter(i++, t.getPurchaseDate());
		query.setParameter(i++, t.getMaxWeight());
		
		query.executeUpdate();
		log.debug("Saving " + t);
		return t;
	}
}
