package com.test.trucktest.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.trucktest.dto.User;

/**Manage user for emails*/
@Service
public class UserService {

	private Logger log = LoggerFactory.getLogger(ProcessFileScheduled.class);
	@PersistenceContext
	EntityManager em;
	
	static private String SELECT = "SELECT name, email FROM t_user limit 1"; 
	
	private static String INSERT = "INSERT INTO t_user (name, email)" + 
			" VALUES (?,?)";
	
	private static String UPDATE = "UPDATE t_user SET name = ?, email = ?";
	
	
	@Autowired
	NotificationService notificationService;
	
	@Transactional
	public User get() {
		
		Query query = em.createNativeQuery(SELECT);
		
		@SuppressWarnings("unchecked")
		List<Object[]> res = query.getResultList();
		
		if (res.size()>0) {
			return new User((String)res.get(0)[0],(String)res.get(0)[1]);
		}else {//create user
			User u = new User();
			u.setEmail("jcass016@gmail.com");
			u.setName("Jesus Castillo");		
			
			query = em.createNativeQuery(INSERT);
			query.setParameter(1, u.getName());
			query.setParameter(2, u.getEmail());
			
			query.executeUpdate();
			
			return u;
		}
		
	}

	
	@Transactional
	public User save(User u) {				
		Query query = em.createNativeQuery(UPDATE);
		query.setParameter(1, u.getName());
		query.setParameter(2, u.getEmail());
		
		query.executeUpdate();
		log.debug("Saving " + u);
		
		notificationService.sendSimpleMessage(u.getEmail(), "En hora buena", "<h3><b>En hora buena " + u.getName() + "!!!</b></h3>\nHas configurado corectamente tu correo");
		return u;
	}
}
