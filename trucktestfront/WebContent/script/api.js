const URL_BASE= 'http://localhost:8080/trucktestbackend/';

var api = {
	getuser: function(){
		return new Promise((resolve, reject) => {
	        $.ajax({
	            url: URL_BASE + 'users/',
	            dataType: 'json',
	            type: 'GET',
	            contentType: 'application/json',
	            success: resolve,
	            error: reject
	        });
	    });		
	},
	
	updateuser: function(user){
		
		let userjson = JSON.stringify(user); 
		console.log(user.name)
		return new Promise((resolve, reject) => {
	        $.ajax({
	            url: URL_BASE + 'users/',
	            dataType: 'json',
	            type: 'PUT',
	            contentType: 'application/json',
	            data: userjson,
	            success: resolve,
	            error: reject
	        });
	    });		
	},
	
	
	loadfile: function(formdata){
		
		
		return new Promise((resolve, reject) => {
			 $.ajax({
	                url: URL_BASE + 'loadfile',
	                type: "PUT",
	                enctype: 'multipart/form-data',
	                data: new FormData(formdata),
	                processData: false,
	                contentType: false,
	                cache: false,
	                async: true,
	                success: resolve,
		            error: reject
	            }
         );
			
	    });		
	},
	
	getfiles: function(){
		return new Promise((resolve, reject) => {
	        $.ajax({
	            url: URL_BASE + 'files/',
	            dataType: 'json',
	            type: 'GET',
	            contentType: 'application/json',
	            success: resolve,
	            error: reject
	        });
	    });		
	},

}

function user(name, email){
	this.name = name;
	this.email = email;
}